#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)
library(DT)
library(plotly)
library(cmapR)
library(shinycssloaders)





# Define UI for application that draws a histogram
ui <- navbarPage(
  tags$style(type="text/css", "body {padding-top: 10px;}"),
 
   tabPanel("Single Gene Analysis",
           sidebarLayout(
             sidebarPanel(
               textInput(inputId = 'gene_id',label = 'Enter a Gene Name',value = "NPY"),
               checkboxInput("log_norm",label="Log Normalized",value = T),
               selectInput(inputId="aggregation_plot",label = "Select an aggregation type"
                           ,choices = c("Mean","Median"),selected = "Mean")
               
             ),
             
             mainPanel(
               withSpinner(plotlyOutput(outputId = 'plot_comparison'))
             )
             
           )
           
  ),
  
  tabPanel("Differential Expression",
           
           # Application title
           
           
           # Sidebar with a slider input for number of bins 
           sidebarLayout(
             sidebarPanel(
               selectInput(inputId = "cancer_type",
                           label = "Cancer Type",
                           choices = NULL),
               
               
               checkboxInput(inputId = "analysis_type1",label = "Normal Vs Tumor",value = T),
               checkboxInput(inputId = "analysis_type2",label = "Among Cancer types",value = F),
               checkboxInput("GTEX_flag","GTEX only",value = T)
             ),
             
             # Show a plot of the generated distribution
             mainPanel(
               tabsetPanel(
                 tabPanel(title = "Differential Expression Results",
                          withSpinner(DT::dataTableOutput("diff_ex_table")),
                          downloadButton("download_table","Download CSV")
                          )
                 
               )
               
             )
           )
           
  )
  
)
